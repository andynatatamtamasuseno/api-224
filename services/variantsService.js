module.exports = exports = (server, pool) => {
    server.post('/api/variants',(req, res, next) => {
        const {CodeVariants, CodeCategories, Initial, Name, Active
        } = req.body; 
        
        // var user = "Tama";
        

        pool.query(
            `INSERT INTO "Variants" ("CodeVariants", "CodeCategories",  "Initial", "Name", "Active",  "CreateBy", "CreateDate")
            VALUES ('${CodeVariants}', '${CodeCategories}','${Initial}','${Name}','${Active}','Tama',Now())`,
            
            (error, result) => {
                console.log(result)
                if (error) {
                    res.send(400, {
                        success: false,
                        error: error

                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        // username: req.decode.username,
                        message: `Has been Save`
                    });
                }

            });

    });


    server.get('/api/variants',  (req, res, next) => {
        pool.query(`SELECT "Id", "CodeVariants", "CodeCategories", "Initial", "Name", "Active", "CreateBy", "CreateDate", "ModifyBy", "ModifyDate"
        FROM public."Variants";`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username: req.decode.username,
                    result: result.rows
                });
            }
        });
    });


    server.get('/api/variants/:id', (req, res, next) => {
        const id = req.params.id;
        pool.query(`SELECT "Id", "CodeVariants", "CodeCategories", "Initial", "Name", "Active", "CreateBy", "CreateDate", "ModifyBy", "ModifyDate"
        from "Variants" where "Id" = ${id}; `, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                if (result.rows.length > 0) {
                    res.send(200, {
                        success: true,
                        // username: req.decode.username,
                        result: result.rows[0]
                    });
                }
                else {
                    res.send(400, {
                        success: false,
                        message: `Category Not Found`
                    });

                }

            }
        });
    });

    server.put('/api/variants/:id', (req, res, next) => {
        const id = req.params.id;
        const { CodeVariants, CodeCategories, Initial, Name, Active, CreateBy
        } = req.body;
        var sql = ``;
        // var user ='Tama';

        // if (CodeVariants) sql += `"CodeVariants"='${CodeVariants}'`;
        if (CodeCategories) sql += (sql.length > 0 ? ', ' : '') + `"CodeCategories"= '${CodeCategories}'`;
        if (Initial) sql += (sql.length > 0 ? ', ' : '') + `"Initial"= '${Initial}'`;
        if (Name) sql += (sql.length > 0 ? ', ' : '') + `"Name"='${Name}'`;
        if (Active != undefined) sql += (sql.length > 0 ? ', ' : '') + `"Active"=${Active}`;
        if (CreateBy) sql += (sql.length > 0 ? ', ' : '') + `"CreateBy"='${CreateBy}'`;


        sql = `UPDATE "Variants" set ` + sql + `, "ModifyBy"= 'Tama', "ModifyDate"= Now() where "Id"= ${id}`;

        console.log(sql);

        pool.query(sql, (error, result) => {
            if (error) {
                console.log(error);
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    message: "Has been update"
                });

            }
        });
    });


    server.del('/api/variants/:id',  (req, res, next) => {
        var id = req.params.id;
        sql = `Delete FROM "Variants"  where "Id"= ${id}`;
        pool.query(sql, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username: req.decode.username,
                    message: "Has been Delete"
                });

            }
        });
    });
}
