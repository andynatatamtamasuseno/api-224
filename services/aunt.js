const jwt= require('jsonwebtoken');

module.exports = exports = (server,pool)=>{
    server.post('/api/login', (req, res, next)=>{
        const {username,password}= req.body;
        const query =` SELECT * FROM users where username = '${username}' AND password ='${password}'`;
        pool.query(query,(error, result)=>{
            if(error){
                res.send(400, {
                    success: false,
                    error:error
                });
            }else{
                if(result.rows.length>0){
                    const token = jwt.sign({
                        fullname: result.rows[0].fullname,
                        username: result.rows[0].username
                    },
                    global.dbPool.jwt_secret,
                    {
                        expiresIn:3600
                    }
                    );
                    
                    res.send(200,{
                        succes:true,
                        user:{
                            fullname: result.rows[0].fullname,
                            active: result.rows[0].active
                        },
                        token:token
                    });
                }
                else{
                    res.send(400,{
                        success:false,
                        message: "User not Found"
                    });
                }
            }
        })
    })
}