module.exports = exports = (server,pool)=>{
    server.post('/api/orderdetail',(req,res,next)=>{
        const { CodeOrderDetail, CodeOrderHeader, CodeProduct, Quantity, Price, Active, CreateBy

        }= req.body;

        sql=` INSERT INTO "OrderDetail" ("CodeOrderDetail", "CodeOrderHeader", "CodeProduct", "Quantity", "Price", "Active", "CreateBy", "CreateDate")
        VALUES ($1,$2,$3,$4,$5,$6,$7,Now())`
        pool.query( sql,[ CodeOrderDetail, CodeOrderHeader, CodeProduct, Quantity, Price, Active, CreateBy],
            (error,result)=>{
                if(error){
                    console.log(error)
                    res.send(400,{
                        success: false,
                        error: error
                    });
                }
                else{
                    res.send(200,{
                        success:true,
                        message: "Hase Been Save"
                    });
                }
            });
    });

    server.get('/api/orderdetail', (reg, res, next) => {
        pool.query(`SELECT "Id", "CodeOrderDetail", "CodeOrderHeader", "CodeProduct", "Quantity", "Price", "Active", 
        "CreateBy", "CreateDate", "ModifyBy", "ModifyDate"
        FROM "OrderDetail";`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                });
            }
        });
    });


    server.get('/api/orderdetail/:id', (req, res, next) => {
        const id = req.params.id;
        pool.query(`SELECT "Id", "CodeOrderDetail", "CodeOrderHeader", "CodeProduct", "Quantity", "Price", 
        "Active", "CreateBy", "CreateDate", "ModifyBy", "ModifyDate"	
        from "OrderDetail" where "Id" = ${id}; `, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                if (result.rows.length > 0) {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]
                    });
                }
                else {
                    res.send(400, {
                        success: false,
                        message: `Category Not Found`
                    });

                }

            }
        });
    });

    server.put('/api/orderdetail/:id', (req, res, next) => {
        const id = req.params.id;
        const {CodeOrderDetail, CodeOrderHeader, CodeProduct, Quantity, Price, Active, CreateBy, ModifyBy
	} = req.body;
        var sql = ``;

        if (CodeOrderDetail) sql += `"CodeOrderDetail"='${CodeOrderDetail}'`;
        if (CodeOrderHeader) sql += (sql.length > 0 ? ', ' : '') + `"CodeOrderHeader"= '${CodeOrderHeader}'`;
        if (CodeProduct) sql += (sql.length > 0 ? ', ' : '') + `"CodeProduct"= '${CodeProduct}'`;
        if (Quantity) sql += (sql.length > 0 ? ', ' : '') + `"Quantity"= ${Quantity}`;
        if (Price) sql += (sql.length > 0 ? ', ' : '') + `"Price"= ${Price}`;
        if (Active != undefined) sql += (sql.length > 0 ? ', ' : '') + `"Active"=${Active}`;
        if (CreateBy) sql += (sql.length > 0 ? ', ' : '') + `"CreateBy"='${CreateBy}'`;
        if (ModifyBy) sql += (sql.length > 0 ? ', ' : '') + `"ModifyBy"='${ModifyBy}'`;
        


        sql = `UPDATE "OrderDetail" set ` + sql + `, "ModifyDate"= Now() where "Id"= ${id}`;

        pool.query(sql,(error,result)=>{
            if (error) 
            {
               console.log(sql);
                res.send(400, {
                    success:false,
                    error: error.detail

                });
            }
            else{
                res.send(200, {
                    success:true,
                    message: "Has been update"
                });

            }
        });
    });


    server.del('/api/orderdetail/:id',(req,res,next)=>{
        var id = req.params.id;
        sql = `Delete FROM "OrderDetail"  where "Id"= ${id}`;
        pool.query(sql,(error,result)=>{
            if (error) 
            {
                console.log(error);
                res.send(400, {
                    success:false,
                    error: error.detail

                });
            }
            else{
                res.send(200, {
                    success:true,
                    message: "Has been Delete"
                });

            }
        });
    });
}