module.exports = exports = (server, pool) => {
    server.post('/api/order', (req, res, next) => {
        const { Details } = req.body;
        getNewReference(newRef => {
            var Amount = 0, Quantity = 0;
            var detQuery = '';
            Details.forEach(detail => {
                Amount = Amount + (detail.Price + detail.Quantity);
                Quantity = Quantity + parseFloat(detail.Quantity);
                detQuery += detQuery.length > 0 ? `,` : ``;
                detQuery += `((SELECT "oh_Id" FROM ins1), '${detail.CodeProduct}', ${detail.Price}, ${detail.Quantity}, true)`

            });

            var query = `WITH ins1 AS(` +
                `INSERT INTO "OrderHeader" ("Reference", "Amount", "Quantity", "Active")` +
                `VALUES ('${newRef}', ${Amount}, ${Quantity}, true) RETURNING "Id" As "oh_Id"` +
                `), ins2 AS (` +
                `INSERT INTO "OrderDetail" ("CodeOrderHeader", "CodeProduct", "Quantity", "Price", "Active")` +
                `VALUES ${detQuery}` +
                `) SELECT * FROM ins1`;
            // console.log(query);

            pool.query(query, (error, result) => {
                if (error) {
                    res.send(404, {
                        success: false,
                        result: {
                            detail: error.detail
                        }
                    });
                }
                else {
                    res.send(201, {
                        success: true,
                        result: result.rows[0]
                    })
                }
            });

        });

    });

    server.get('/api/reference/:idRef', (req, res, next) => {
        var idRef = req.params.idRef;
        var sql = `SELECT od.* FROM "OrderHeader" as oh join "OrderDetail" as od ON
        oh."Id"= od."CodeOrderHeader" join "Products" as pr on od."CodeProduct" = pr."CodeProduct"
        where "Reference"= '${idRef}'; `
        console.log(sql);
        pool.query(sql, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                if (result.rows.length > 0) {
                    res.send(200, {
                        success: true,
                        reference: idRef,
                        Active: result.rows[0].Active,
                        result: result.rows
                    });
                }
                else {
                    res.send(400, {
                        success: false,
                        message: `Category Not Found`
                    });

                }

            }
        });

    });

    function getNewReference(callback) {
        var year = new Date().getFullYear().toString().substr(-2);
        var month = ("0" + (new Date().getMonth() + 1)).slice(-2);
        var newRef = `SLS-${year}${month}-`;
        var query = `SELECT "Reference" From "OrderHeader"` +
            `where "Reference" like '${newRef}%'` +
            `Order by "Reference" Desc Limit 1`;
        try {
            pool.query(query, (error, result) => {
                if (error) {
                    return {
                        success: {
                            detail: error.detail
                        }
                    }
                }
                else {
                    if (result.rows.length > 0) {
                        var refArr = result.rows[0].Reference.split(`-`);
                        newRef = newRef + ("0000" + (parseInt(refArr[2]) + 1).toString()).slice(-4);
                        return callback(newRef);
                    } else {
                        newRef = newRef + "0001";
                        return callback(newRef);
                    }
                }
            });
        } catch (error) {
            return {
                success: false,
                return: error
            }
        }
    }

    server.get('/api/order/:ref', (req, res, next) => {
        const ref = req.params.ref;
        const query = `SELECT * FROM "OrderHeader" WHERE "Reference"= '${ref}' `;
        pool.query(query, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error

                });
            }
            else {
                if (result.rows.length > 0) {
                    const { Id, Reference, Amount, Quantity } = result.rows[0];
                    getDetails(Id, detail => {
                        res.send(200, {
                            success: true,
                            result: {
                                Reference: Reference,
                                Amount: Amount,
                                Quantity: Quantity,
                                Details: detail
                            }
                        });
                    });
                }

            }
        });
    });

    server.get('/api/products/:page/:perpage/:ordby/:sort/:filter', (req, res, next) => {
        const { page, perpage, ordby, sort,filter } = req.params;
        const qfilter = filter ? `WHERE "Initial" ILIKE '%${filter}%' OR "Name" ILIKE '%${filter}%' `: ``;
        const qCount = `SELECT COUNT(*) AS "Cnt" FROM "Products" ${qfilter}`;
        
        const orderBy = (ordby ? `ORDER BY "${ordby}" `+ (sort ?`DESC`: ``):``);
        getProductsCount(qCount, rowCount => {
            const query = `SELECT * FROM "Products" ${qfilter} ${orderBy} LIMIT ${perpage} OFFSET ${(page - 1) * perpage}`;
            pool.query(query, (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        error: error
                    });
                } else {
                    const realNum = parseInt(rowCount / perpage);
                    var pageNum = realNum;
                    if ((rowCount / perpage) - realNum > 0) {
                        pageNum += 1;
                    }
                    res.send(200, {
                        success: true,
                        PageNum: pageNum,
                        result: result.rows
                    });
                }
            });
        })
    });



    function getProductsCount(query,callback) {
        
        pool.query(query, (error, result) => {
            if (error) {
                return callback(0);
            } else {
                return callback(result.rows[0].Cnt);
            }

        });
    }

    function getProductsSort(parm,desc,callback){
        var sql= `SELECT * FROM "Products" ORDER BY ${param} ${desc}`;
        pool.query(query, (error, result) => {
            if (error) {
                return callback(0);
            } else {
                return callback(result.rows[0].Cnt);
            }

        });
    }

    function getDetails(ohId, callback) {
        const query = `SELECT od.*, pr."Initial", pr."Name"
        FROM "OrderDetail" od 
        JOIN "Products" pr 
        ON od."CodeProduct" = pr."CodeProduct"
        WHERE "CodeOrderHeader"=  ${ohId};`
        const phi = 13.14;

        console.log(phi);
        pool.query(query, (error, result) => {
            if (error) {
                return callback([]);
            }
            else {
                return callback(result.rows);
            }

        });
        console.log(query);
    }
}