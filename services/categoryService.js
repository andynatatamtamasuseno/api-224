module.exports = exports = (server, pool) => {
    server.post('/api/categories',  (req, res, next) => {
        const { Code, Initial, Name, Active } = req.body;
        var user = "Tama";
        pool.query(`INSERT INTO "Categories" ("Code", "Initial" , "Name", "Active", "CreateBy","CreateDate") VALUES 
        ('${Code}','${Initial}','${Name}',${Active}, '${user}', NOW())`, (error, result) => {
                if (error) {
                    console.log(error);
                    res.send(400, {
                        success: false,
                        error: error.detail

                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        // username: req.decode.username,
                        message: `Has been Save`,
                    });
                }

            });

    });


    server.get('/api/categories', (req, res, next) => {
        pool.query(`SELECT "Id", "Code", "Initial", "Name", "Active", "CreateBy", "CreateDate", "ModifyBy", "ModifyDate" FROM "Categories"; `, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username: req.decode.username,
                    result: result.rows
                });
            }
        });
    });


    server.get('/api/categories/:id', (req, res, next) => {
        const id = req.params.id;
        pool.query(`SELECT "Id", "Code", "Initial", "Name", "Active","CreateBy", "CreateDate", "ModifyBy", "ModifyDate" FROM "Categories" where "Id" = ${id}; `, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                if (result.rows.length > 0) {
                    res.send(200, {
                        success: true,
                        // username: req.decode.username,
                        result: result.rows[0]
                    });
                }
                else {
                    res.send(400, {
                        success: false,
                        message: `Category Not Found`
                    });

                }

            }
        });
    });

    server.put('/api/categories/:id',  (req, res, next) => {
        const id = req.params.id;
        const { Code, Initial, Name, Active } = req.body;
        var sql = ``;
        // var user= req.decode.username;

        // if (Code) sql += `"Code"=${Code}`;
        if (Initial) sql += (sql.length > 0 ? ', ' : '') + `"Initial"= '${Initial}'`;
        if (Name) sql += (sql.length > 0 ? ', ' : '') + `"Name"='${Name}'`;
        if (Active != undefined) sql += (sql.length > 0 ? ', ' : '') + `"Active"=${Active}`;

        sql = `UPDATE "Categories" set ` + sql + `, "ModifyBy"='Tama', "ModifyDate"= Now() where "Id"= ${id}`;

        pool.query(sql, (error, result) => {
            if (error) {
                console.log(error);
                
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username:req.decode.username,
                    message: "Has been update"
                });

            }
        });
    });


    server.del('/api/categories/:id', (req, res, next) => {
        var id = req.params.id;
        sql = `Delete FROM "Categories"  where "Id"= ${id}`;
        pool.query(sql, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username: req.decode.username,
                    message: "Has been Delete"
                });

            }
        });
    });
}
