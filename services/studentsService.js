var studentsList = [
    { id: 1, firstName: 'Andynata', lastName: 'Suseno' },
    { id: 2, firstName: 'Tamtama', lastName: 'Suseno' }
]
module.exports = exports = (server) => {
    server.get('/api/students', (req, res, next) => {
        res.send(200, {
            success: true,
            data: studentsList
        })
    });
    server.get('/api/student/:id',(req,res,next)=>{
        var id = req.params.id;
        var student= studentsList.find(s=>s.id == id);
        if(student){
            res.send(200,{
                success:true,
                data: student
            })
        } else{
            res.send(400,{
                success:false,
                data: student
            })
        }
    });
    server.post('/api/student',(req,res,next)=>{
        const {firstName, lastName} = req.body;
        var newId = Math.max.apply(Math, studentsList.map(s=>{return s.id}))+1;
        var student ={
            id:newId,
            firstName: firstName,
            lastName: lastName
        }
        studentsList.push(student);
        res.send(200,{
            success:true,
            data: student
        })
    });

    server.put('/api/student/:id',(req,res,next)=>{
        var id = req.params.id;
        const {firstName,lastName}= req.body;
        var student= studentsList.find(s=>s.id == id);
        if(student){
            if(firstName) student.firstName = firstName;
            if(lastName)student.lastName=lastName;
            res.send(200,{
                success:true,
                data: student
            })
        } else{
            res.send(400,{
                success:false,
                data: "Student Not Found"
            })
        }
    });

    server.del('/api/student/:id',(req,res,next)=>{
        var id = req.params.id;
        var idx= studentsList.findIndex(s=>s.id == id);
        if(idx>-1){
            studentsList.splice(idx,1);
            res.send(200,{
                success: true,
                message: 'has been deleted'
            });
        }else{
            res.send(400,{
                success: false,
                message: 'Not Found'
            });
        }
    });
}