module.exports = exports = (server, pool) => {
    server.post('/api/products',(req, res, next) => {
        const { CodeProduct, Initial, Name, Description, Price, Active, Stock
        } = req.body;
        // var user = req.decode.username;
        pool.query(
            `INSERT INTO "Products" ("CodeProduct", "Initial", "Name", "Description", "Price", "Active",  "Stock", "CreateBy","CreateDate")
            VALUES (${CodeProduct},${Initial},${Name},${Description},${Price},${Active},${Stock},'Tama',NOW())`,
            [CodeProduct, Initial, Name, Description, Price, Active, Stock],
            (error, result) => {
                if (error) {
                    res.send(400, {
                        success: false,
                        error: error

                    });
                }
                else {
                    res.send(200, {
                        success: true,
                        // username: $user,
                        message: `Has been Save`
                    });
                }

            });

    });


    server.get('/api/products', (reg, res, next) => {
        pool.query(`SELECT "Id", "CodeProduct", "Initial", "Name", "Description", "Price", 
        "Active", "CreateBy", "CreateDate", "ModifyBy", "ModifyDate", "Stock"
        FROM "Products";`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username:req.decode.username,
                    result: result.rows
                });
            }
        });
    });


    server.get('/api/products/:id',(req, res, next) => {
        const id = req.params.id;
        pool.query(`SELECT "Id", "CodeProduct", "Initial", "Name", "Description", "Price", "Active", "CreateBy", "CreateDate", "ModifyBy", "ModifyDate", "Stock"
        from "Products" where "Id" = ${id}; `, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                if (result.rows.length > 0) {
                    res.send(200, {
                        success: true,
                        // username:req.decode.username,
                        result: result.rows[0]
                    });
                }
                else {
                    res.send(400, {
                        success: false,
                        message: `Category Not Found`
                    });

                }

            }
        });
    });

    server.put('/api/products/:id',(req, res, next) => {
        const id = req.params.id;
        const { CodeProduct, Initial, Name, Description, Price, Active, CreateBy, Stock

        } = req.body;
        var sql = ``;
        // var user= req.decode.username;

        if (CodeProduct) sql += `"CodeProduct"='${CodeProduct}'`;
        if (Initial) sql += (sql.length > 0 ? ', ' : '') + `"Initial"= '${Initial}'`;
        if (Name) sql += (sql.length > 0 ? ', ' : '') + `"Name"='${Name}'`;
        if (Description) sql += (sql.length > 0 ? ', ' : '') + `"Description"='${Description}'`;
        if (Price) sql += (sql.length > 0 ? ', ' : '') + `"Price"=${Price}`;
        if (Active != undefined) sql += (sql.length > 0 ? ', ' : '') + `"Active"=${Active}`;
        if (CreateBy) sql += (sql.length > 0 ? ', ' : '') + `"CreateBy"='${CreateBy}'`;
        if (Stock) sql += (sql.length > 0 ? ', ' : '') + `"Stock"=${Stock}`;

        sql = `UPDATE "Products" set ` + sql + `,ModifyBy='Tama', "ModifyDate"= Now() where "Id"= ${id}`;

        pool.query(sql, (error, result) => {
            if (error) {
                console.log(sql);
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username: $user,
                    message: "Has been update"
                });

            }
        });
    });


    server.del('/api/products/:id',(req, res, next) => {
        var id = req.params.id;
        sql = `Delete FROM "Products"  where "Id"= ${id}`;
        pool.query(sql, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    // username: req.decode.username,
                    message: "Has been Delete"
                });

            }
        });
    });
}
