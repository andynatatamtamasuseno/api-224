module.exports = exports = (server,pool)=>{
    server.post('/api/orderheader',(req,res,next)=>{
        const { CodeOrderHeader, Reference, Amount, Active, CreateBy, Quantity
        }= req.body;

        sql=` INSERT INTO "OrderHeader" ("CodeOrderHeader", "Reference", "Amount", "Active", "CreateBy",  "Quantity","CreateDate")
        VALUES ($1,$2,$3,$4,$5,$6,Now())`
        pool.query( sql,[CodeOrderHeader, Reference, Amount, Active, CreateBy, Quantity],
            (error,result)=>{
                if(error){
                    res.send(400,{
                        success: false,
                        error: error
                    });
                }
                else{
                    res.send(200,{
                        success:true,
                        message: "Hase Been Save"
                    });
                }
            });
    });

    server.get('/api/orderheader', (reg, res, next) => {
        pool.query(`SELECT "Id","CodeOrderHeader", "Reference", "Amount", "Active", "CreateBy","CreateDate","ModifyBy", "ModifyDate","Quantity"
        FROM "OrderHeader";`, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                res.send(200, {
                    success: true,
                    result: result.rows
                });
            }
        });
    });


    server.get('/api/orderheader/:id', (req, res, next) => {
        const id = req.params.id;
        pool.query(`SELECT "Id", "CodeOrderHeader", "Reference", "Amount", "Active", "CreateBy","CreateDate","ModifyBy", "ModifyDate","Quantity"
        from "OrderHeader" where "Id" = ${id}; `, (error, result) => {
            if (error) {
                res.send(400, {
                    success: false,
                    error: error.detail

                });
            }
            else {
                if (result.rows.length > 0) {
                    res.send(200, {
                        success: true,
                        result: result.rows[0]
                    });
                }
                else {
                    res.send(400, {
                        success: false,
                        message: `Category Not Found`
                    });

                }

            }
        });
    });

    server.put('/api/orderheader/:id', (req, res, next) => {
        const id = req.params.id;
        const {CodeOrderHeader, Reference, Amount, Active, CreateBy,ModifyBy, Quantity
	} = req.body;
        var sql = ``;

        if (CodeOrderHeader) sql += `"CodeOrderHeader"='${CodeOrderHeader}'`;
        if (Reference) sql += (sql.length > 0 ? ', ' : '') + `"Reference"= '${Reference}'`;
        if (Amount) sql += (sql.length > 0 ? ', ' : '') + `"Amount"= ${Amount}`;
        if (Active != undefined) sql += (sql.length > 0 ? ', ' : '') + `"Active"=${Active}`;
        if (CreateBy) sql += (sql.length > 0 ? ', ' : '') + `"CreateBy"='${CreateBy}'`;
        if (ModifyBy) sql += (sql.length > 0 ? ', ' : '') + `"ModifyBy"='${ModifyBy}'`;
        if (Quantity) sql += (sql.length > 0 ? ', ' : '') + `"Quantity"= ${Quantity}`;


        sql = `UPDATE "OrderHeader" set ` + sql + `, "ModifyDate"= Now() where "Id"= ${id}`;

        pool.query(sql,(error,result)=>{
            if (error) 
            {
               console.log(sql);
                res.send(400, {
                    success:false,
                    error: error.detail

                });
            }
            else{
                res.send(200, {
                    success:true,
                    message: "Has been update"
                });

            }
        });
    });


    server.del('/api/orderheader/:id',(req,res,next)=>{
        var id = req.params.id;
        sql = `Delete FROM "OrderHeader"  where "Id"= ${id}`;
        pool.query(sql,(error,result)=>{
            if (error) 
            {
                console.log(error);
                res.send(400, {
                    success:false,
                    error: error.detail

                });
            }
            else{
                res.send(200, {
                    success:true,
                    message: "Has been Delete"
                });

            }
        });
    });
}