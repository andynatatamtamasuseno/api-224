const restify = require('restify');
const corsMidware = require('restify-cors-middleware')

const server = restify.createServer({
    name: 'XPOS',
    version: '1.0.0'
});

server.use(restify.plugins.bodyParser(
    {
        mapParams: false
    }
));

const cors = corsMidware({
    origins : ['*'],
    allowHeaders: ['X-app-Version'],
    exposeHeaders:[]
});

server.pre(cors.preflight);
server.use(cors.actual);





server.get('/', (req, res, next) => {
    var body =
        `<html>
            <body>
                <h3>Xpos API Web 
            </body>
        </html>`;
    res.writeHead(200, {
        'Content-Lenght': Buffer.byteLength(body),
        'Content-Type': 'text/html'
    });
    res.write(body);
    res.end();
});



// require('./services/studentsService')(server);
// server.listen(5000,()=>{
//     console.log(`${new Date()} ${server.name} listen at`)
// })

// Global Configuration
global.dbPool = require('./configurations/config');
global.verifyToken = require('./base/verifyToken');

require('./services/studentsService')(server);
require('./services/categoryService')(server, global.dbPool.pool);
require('./services/variantsService')(server, global.dbPool.pool);
require('./services/productsService')(server, global.dbPool.pool);
require(`./services/orderHeaderService`)(server, global.dbPool.pool);
require(`./services/orderDetailService`)(server, global.dbPool.pool);
require(`./services/orderService`)(server, global.dbPool.pool);
require(`./services/aunt`)(server, global.dbPool.pool);

server.listen(3000, () => {
    console.log(`${new Date()} ${server.name} listen at ${server.url}`);
});

