const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers['x-access-token'] || req.body.token || req.query.token;
        if (token) {
            jwt.verify(token, global.dbPool.jwt_secret, (error, decode) => {
                if (error) {
                    return res.send(403, {
                        success: false,
                        message: 'No Authentification',
                        error: error
                    });
                }
               
                req.decode = decode;
                next();
            });

        } else {
            return res.send(403, {
                succes: false,
                message: 'token not found'
            });
        }
    } catch (error) {
        return res.send(403, {
            succes: false,
            error: 'Token Request'

        });
    }
}